<?php
/**
 * Created by PhpStorm.
 * UserController: SimonaThrussell
 * Date: 31/12/2018
 * Time: 11:31
 */

namespace Core;
use App\Models\RebuyModel;

class Offer
{

    public $offer;
    public $order_id;
    public $device_id;
    public $date;
    public $offer_type;
    public $accepted;


    public function __construct($offer,$order_id,$device_id,$date,$offer_type,$accepted)
    {
        $this->offer=$offer;
        $this->order_id=$order_id;
        $this->device_id=$device_id;
        $this->date=$date;
        $this->offer_type=$offer_type;
        $this->accepted=$accepted;

        $this->makerebuyoffer($this->offer,$this->order_id,$this->device_id,$this->date,$this->offer_type,$this->accepted);
    }

    public function makerebuyoffer()
    {
        $newoffer=RebuyModel::makeRebuyOffer($this->offer,$this->order_id,$this->device_id,$this->date,$this->offer_type,$this->accepted);

    }

}
