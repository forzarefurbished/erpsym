<?php
/**
 * Created by PhpStorm.
 * UserController: darke
 * Date: 31/10/2018
 * Time: 20:03
 */


namespace App\Models;
use PDO;

class RMAModel extends \Core\Model
{


    public static function getWarranty($imei)
    {

            try {
                $db = static::getDB();
                $stmt = $db->prepare("SELECT * FROM forzaerp_sales_order AS r 
                JOIN forzaerp_warranty as w on r.order_id=w.order_id
                JOIN forzaerp_sales_order_device as d on d.order_id=r.order_id
                JOIN forzaerp_device_type as t on d.device_type_id=t.device_id
                JOIN forzaerp_device_storage_type as s on d.device_storage_id=s.storage_type_id
                JOIN forzaerp_connection_type as c on d.device_connection_id=c.connection_type_id
                
                WHERE w.device_imei=?");
                $stmt->execute([$imei]);
                $results = $stmt->fetchAll();
                //return $results;
                $count = $stmt->rowCount();
                return $results;
            } catch (\PDOException $e) {
                echo $e->getMessage();
            }



    }

    public static function makeRMADetails($rma_id,$problem_id,$comments,$date)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO
            `forzaerp_rma_order_details` (`rma_id`,`problem_id`,`comments`,`date`) 
            VALUES (?,?,?,?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$rma_id,$problem_id,$comments,$date]);
            //$stmt = null;
            $message="rma details entered";
            return $message;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }



    }

    public static function checkIMEI($imei)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare("SELECT * FROM forzaerp_warranty WHERE device_imei=?");
            $stmt->execute([$imei]);

            $count = $stmt->rowCount();
            return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

     public static function getRMAs()
     {
         try {
             $db = static::getDB();
             $stmt = $db->query('SELECT * FROM forzaerp_rma_order AS r 
            JOIN forzaerp_customer AS c ON r.customer_id=c.customer_id 
            JOIN forzaerp_rma_order_details as p on r.rma_id=p.rma_id
           JOIN forzaerp_rma_problem_code as d on p.problem_id=d.problem_id
           JOIN forzaerp_rma_order_status as s on r.rma_id=s.rma_id
           JOIN forzaerp_rma_order_status_types as t on s.status_id=t.status_id
            ORDER BY rma_date');
             $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
             return $results;
         } catch (\PDOException $e) {
             echo $e->getMessage();
         }
     }

     public static function createStatus($rma_id)
     {
         try {
             $db = static::getDB();
             $sql = "INSERT INTO
            forzaerp_rma_order_status (`order_id`,`status_id`,`shipping_status_id`)
            VALUES ($rma_id,1,1)";
             $stmt = $db->prepare($sql);
             $stmt->execute([$rma_id,1,1]);
             //$stmt = null;
             $message="RMAController order status entered";
             return $message;

         } catch (\PDOException $e) {
             echo $e->getMessage();
         }


     }

     public static function makeRMAInspection($IMEI,$fcfc,$front_camera,$front_mic,$proximity_sensor,$auto_brightness,$ambient_light,$front_speaker,$touch_id,$home_button,$display_image_quality,$display_multi_touch,$physical_damage,$power,$battery,$dock_connector,$charging,$headset_jack,$rear_speaker,$vibration,$power_flex_cable,$power_button,$microphone_back,$torch,$rear_camera,$volume_flex_cable,$SIM_fail,$No_Connection,$Signal_Strength,$Wifi,$Bluetooth,$GPS,$date)
     {
         try {
             $db = static::getDB();
             $sql = "INSERT INTO
            forzaerp_inspection_details (`IMEI`,`front_camera_flex_cable`,`front_camera`,`front_mic`,`proximity_sensor`,`auto_brightness`,`ambient_light`,`front_speaker`,`touch_id`,
            `home_button`,`display_image_quality`,`display_multi_touch`,`physical_damage`,`power`,`battery`,`dock_connector`,`charging`,`headset_jack`,`rear_speaker`,`vibration`,`power_flex_cable`,`power_button`,
            `microphone_back`,`torch`,`rear_camera`,`volume_flex_cable`,`SIM_fail`,`No_Connection`,`Signal_Strength`,`Wifi`,`Bluetooth`,`GPS`,`date`)
            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
             $stmt = $db->prepare($sql);
             $stmt->execute([$IMEI,$fcfc,$front_camera,$front_mic,$proximity_sensor,$auto_brightness,$ambient_light,$front_speaker,$touch_id,$home_button,$display_image_quality,$display_multi_touch,$physical_damage,$power,$battery,$dock_connector,$charging,$headset_jack,$rear_speaker,$vibration,$power_flex_cable,$power_button,$microphone_back,$torch,$rear_camera,$volume_flex_cable,$SIM_fail,$No_Connection,$Signal_Strength,$Wifi,$Bluetooth,$GPS,$date]);
             //$stmt = null;
             $message="inspection details entered";
             return $message;

         } catch (\PDOException $e) {
             echo $e->getMessage();
         }


     }

    public static function makeRMASoundInspection($rma_id,$IMEI,$date,$speakers,$internal_speaker,$microphone_bottom, $microphone_back,$front_speaker,$microphone_top)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO
            forzaerp_rma_inspection_details_sound (`rma_id`,`imei`,`date`,`speakers`,`internal_speakers`,`microphone_bottom`,
            `microphone_back`,`front_speaker`,`microphone_top`)
            VALUES (?,?,?,?,?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$rma_id,$IMEI,$date,$speakers,$internal_speaker,$microphone_bottom, $microphone_back,$front_speaker,$microphone_top]);
            //$stmt = null;
            $message="Sound inspection details entered";
            return $message;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }


    }

    public static function getIMEI($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare("SELECT * FROM forzaerp_rma_order
            WHERE rma_id=?");
            $stmt->execute([$rma_id]);
            $results = $stmt->fetchAll();
            return $results[0][3];
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function getRMA($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_order AS r 
            JOIN forzaerp_rma_order_details as p on r.rma_id=p.rma_id
           JOIN forzaerp_rma_problem_code as d on p.problem_id=d.problem_id
            WHERE r.rma_id=?');
            $stmt->execute([$rma_id]);

            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function makeRMAPowerInspection($rma_id,$IMEI,$date,$battery,$dock_connector,$power)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO
            forzaerp_rma_inspection_details_power (`rma_id`,`device_imei`,`date`,`battery`,`dock_connector_cable`,`no_power`)
            VALUES (?,?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$rma_id,$IMEI,$date,$battery,$dock_connector,$power]);
            //$stmt = null;
            $message="Power inspection details entered";
            return $message;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }


    }

    public static function makeRMAScreenInspection($rma_id,$IMEI,$date,$LCD,$multi_touch,$img_quality,$ambient_light,$auto_brightness,$proximity)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO
            forzaerp_rma_inspection_details_screen (`rma_id`,`device_imei`,`date`,`LCD`,`multi_touch`,`image_quality`,`ambient_light`,`auto_brightness`,`proximity`)
            VALUES (?,?,?,?,?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$rma_id,$IMEI,$date,$LCD,$multi_touch,$img_quality,$ambient_light,$auto_brightness,$proximity]);
            //$stmt = null;
            $message="Screen inspection details entered";
            return $message;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }


    }

    public static function makeRMAButtonsInspection($rma_id,$IMEI,$date,$headset_jack,$power_button,$volume_flex_cable,$home_button,$touch_id)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO
            forzaerp_rma_inspection_details_buttons (`rma_id`,`device_imei`,`date`,`headset_jack`,`power_button`,`volume_flex_cable`,`home_button`,touch_id)
            VALUES (?,?,?,?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$rma_id,$IMEI,$date,$headset_jack,$power_button,$volume_flex_cable,$home_button,$touch_id]);
            //$stmt = null;
            $message="Buttons/Jacks inspection details entered";
            return $message;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }


    }

    public static function makeRMAConnsInspection($rma_id,$imei,$date,$wifi_bt,$signal_strength,$no_cell_conn,$SIM_fail)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO
            forzaerp_rma_inspection_details_connections (`rma_id`,`device_imei`,`date`,`wifi_bt`,`signal_strength`,`no_cell_conn`,`SIM_fail`)
            VALUES (?,?,?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$rma_id,$imei,$date,$wifi_bt,$signal_strength,$no_cell_conn,$SIM_fail]);
            //$stmt = null;
            $message="Connections inspection details entered";
            return $message;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }


    }

    public static function makeRMAMiscInspection($rma_id,$imei,$date,$vibration_motor,$GPS,$torch)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO
            forzaerp_rma_inspection_details_misc (`rma_id`,`device_imei`,`date`,`vibration_motor`,`GPS`,`torch`)
            VALUES (?,?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$rma_id,$imei,$date,$vibration_motor,$GPS,$torch]);
            //$stmt = null;
            $message="Miscellaneous inspection details entered";
            return $message;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }


    }

    public static function makeRMACameraInspection($rma_id,$imei,$date,$rear_camera,$front_camera,$fcfc)
    {
        try {
            $db = static::getDB();
            $sql = "INSERT INTO
            forzaerp_rma_inspection_details_camera (`rma_id`,`device_imei`,`date`,`rear_camera`,`front_camera`,`front_camera _flex_cable`)
            VALUES (?,?,?,?,?,?)";
            $stmt = $db->prepare($sql);
            $stmt->execute([$rma_id,$imei,$date,$rear_camera,$front_camera,$fcfc]);
            //$stmt = null;
            $message="Camera inspection details entered";
            return $message;

        } catch (\PDOException $e) {
            echo $e->getMessage();
        }


    }

    public static function getInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_sound as s 
            JOIN forzaerp_rma_inspection_details_screen as d on s.rma_id=d.rma_id
            JOIN forzaerp_rma_inspection_details_power as p on s.rma_id=p.rma_id
            JOIN forzaerp_rma_inspection_details_mics as m on m.rma_id=s.rma_id
            JOIN forzaerp_rma_inspection_details_connections as c on s.rma_id=c.rma_id
            JOIN forzaerp_rma_inspection_details_camera as i on i.rma_id=s.rma_id
            JOIN forzaerp_rma_inspection_details_buttons as b on b.rma_id=s.rma_id
            WHERE s.rma_id=?');
            $stmt->execute([$rma_id]);
            $count = $stmt->rowCount();
           // $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //return $results;
            return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function checkSoundInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_sound 
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            $count = $stmt->rowCount();
            // $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //return $results;
            return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getSoundInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_sound 
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            //$count = $stmt->rowCount();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
            //return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function checkScreenInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_screen 
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            $screencount = $stmt->rowCount();
            // $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //return $results;
            return $screencount;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function getScreenInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_screen
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            //$count = $stmt->rowCount();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
            //return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function checkPowerInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_power 
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            $screencount = $stmt->rowCount();
            // $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //return $results;
            return $screencount;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function getPowerInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_power
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            //$count = $stmt->rowCount();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
            //return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function checkMiscInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_misc 
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            $screencount = $stmt->rowCount();
            // $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //return $results;
            return $screencount;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function getMiscInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_misc
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            //$count = $stmt->rowCount();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
            //return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function checkConnInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_connections 
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            $screencount = $stmt->rowCount();
            // $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //return $results;
            return $screencount;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function getConnInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_connections
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            //$count = $stmt->rowCount();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
            //return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function checkCameraInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_camera 
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            $screencount = $stmt->rowCount();
            // $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //return $results;
            return $screencount;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function getCameraInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_camera
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            //$count = $stmt->rowCount();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
            //return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function checkButtonsInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_buttons 
            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            $screencount = $stmt->rowCount();
            // $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            //return $results;
            return $screencount;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function getButtonsInspection($rma_id)
    {
        try {
            $db = static::getDB();
            $stmt = $db->prepare('SELECT * FROM forzaerp_rma_inspection_details_buttons

            
            WHERE rma_id=?');
            $stmt->execute([$rma_id]);
            //$count = $stmt->rowCount();
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
            //return $count;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function setRMAStatus($status, $rma_id)
    {
        try {
            $db = static::getDB();
            $sql = "UPDATE forzaerp_rma_order_status SET `status_id` = ? WHERE `rma_id` = ?";
            $db->prepare($sql)->execute([$status, $rma_id]);
            $message="status updated";
            return $message;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }

    }

    public static function getRMAsForInspection()
    {
        try {
            $db = static::getDB();
            $stmt = $db->query('SELECT * FROM forzaerp_rma_order AS r 
            JOIN forzaerp_customer AS c ON r.customer_id=c.customer_id 
            JOIN forzaerp_rma_order_details as p on r.rma_id=p.rma_id
           JOIN forzaerp_rma_problem_code as d on p.problem_id=d.problem_id
           JOIN forzaerp_rma_order_status as s on r.rma_id=s.rma_id
           JOIN forzaerp_rma_order_status_types as t on s.status_id=t.status_id
           WHERE s.status_id=3');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public static function getShipping()
    {
        try {
            $db = static::getDB();
            $stmt = $db->query('SELECT * FROM forzaerp_rma_order_status AS r 
            JOIN forzaerp_rebuy_shipping_status AS d ON r.shipping_status_id=d.shipping_status_id
          ');
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }


    }

}