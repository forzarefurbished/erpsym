<?php
/**
 * Created by PhpStorm.
 * UserController: SimonaThrussell
 * Date: 01/10/2018
 * Time: 09:30
 */

namespace App\Controllers;
use \Core\View;
use App\Models\CustomerSupportModel;
require '..\vendor\autoload.php';


class CSController extends \Core\Controller
{
    public function indexAction()
    {

        //$results = CustomerSupportModel::getOrders();
        View::renderTemplate('CSController/index.html'
            //['results'=>$results]
        );

    }
}