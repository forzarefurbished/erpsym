<?php
/**
 * Created by PhpStorm.
 * UserController: darke
 * Date: 31/10/2018
 * Time: 20:04
 */

namespace App\Controllers;
use App\Models\DeviceModel;
use App\Models\OrderModel;
use App\Models\CustomerModel;
use \Core\View;
use \Core\Customer;
use \Core\Order;
use App\Models\RMAModel;
use App\Models\RepairModel;
use App\Models\EventModel;
use \Core\Device;

require '..\vendor\autoload.php';
class RMAController extends \Core\Controller
{
    public function index()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RMAController/index.html'
        //, [
        //'results' => $results
        //]
        );

    }

    public function activate()
    {
        $id = $this->route_params['id'];
        return $id;
    }

    public function check()
    {
        //$results = RMAModel::getOrders();
        View::renderTemplate('RMAController/check.html'
        //, [
        //'results' => $results
        //]
        );

    }

    public function reports()
    {

        $results = RMAModel::getRMAs();
        View::renderTemplate('RMAController/reports.html'
        , [
        'results' => $results
        ]
        );

    }

    public function inspection()
    {
       $results = RMAModel::getRMAsForInspection();
        View::renderTemplate('RMAController/inspection.html'
       , [
        'results' => $results
        ]
        );


    }

    public function rmainspect()
    {

        $rma_id=$this->activate();
        $imei=RMAModel::getIMEI($rma_id);
        $_SESSION['imei']=$imei;
       $results=RMAModel::getRMA($rma_id);
        View::renderTemplate('RMAController/rmainspect.html'
         , [
        'results' => $results
         ]
        );

    }

    public function repair()
    {

        $results = RMAModel::getRMAs();
        View::renderTemplate('RMAController/repair.html'
        , [
        'results' => $results
        ]
        );

    }



    public function inventory()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RMAController/inventory.html'
        //, [
        //'results' => $results
        //]
        );

    }

    public function mail()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RMAController/mail.html'
        //, [
        //'results' => $results
        //]
        );

    }

    public function checkIMEI()
    {

            $imei = $_POST['imei'];
            $_SESSION['imei']=$imei;
           $count=RMAModel::checkIMEI($imei);
           if($count!=0) {
               $results = RMAModel::getWarranty($imei);
               View::renderTemplate('RMAController/checkIMEI.html'
                   , [
                       'results' => $results]
               );
           }else{
               View::renderTemplate('RMAController/checkfailed.html');
           }




    }

    public function history()
    {

        $imei=$this->activate();
        $results=DeviceModel::getHistory($imei);
        View::renderTemplate('RMAController/history.html'
        , [
        'results' => $results
        ]
        );

    }


    public function steps()
{
    View::renderTemplate('RMAController/steps.html'
    //, [
    //'results' => $results
    //]
    );

}

    public function testorder()
    {
        $imei=$this->activateAction();
        //$results=DeviceModel::getDeviceByImei($imei);
        View::renderTemplate('RMAController/testorder.html'
       //, [
        //'results' => $results
       // ]
        );

    }


    public function rmasubmit()
    {
        $imei=$this->activate();
        //$results=DeviceModel::getDeviceByImei($imei);
        View::renderTemplate('RMAController/rmasubmit.html'
        //, [
        //'results' => $results
        // ]
        );

        echo '<pre>';
        if (isset($_POST['submit'])) {
            $firstname=$_POST['first_name'];
            $lastname=$_POST['last_name'];
            $email=$_POST['email'];
            $phone=$_POST['phone'];
            $customer_type=$_POST['customer_type'];
            $postcode=$_POST['postcode'];
            $streetnumber=$_POST['street_number'];
            $addition=$_POST['addition'];
            $streetname=$_POST['street_name'];
            $city=$_POST['city'];
            $country=$_POST['country'];
            $problem_id=$_POST['Options'];
            $comment=$_POST['comment'];
            $date=date('Y-m-d');
        } else {
            echo 'no data entered';
        }
        $check = $_POST['check'];


        $N = count($check);
        if ($N == 3) {
            print_r($_POST);
            $customer_id= CustomerModel::createCustomer($firstname,$lastname,$email,$phone, $customer_type);
            echo '<pre>';
            echo 'Created ' .$customer_id;
            echo $query=CustomerModel::createAddress($customer_id,$postcode,$streetnumber,$addition,$streetname,$city,$country);
            $device_id=DeviceModel::getDeviceId($imei);
            echo $rma_id=OrderModel::makeRMAOrder($customer_id,$date,$device_id,$imei);
            echo $details=RMAModel::makeRMADetails($rma_id,$problem_id,$comment,$date);
            echo $status=RMAModel::createStatus($rma_id);
        }else {
            if (empty($_POST['check']['0'])) {

                die("please ensure you have removed ICloud lock and turned off Find My Iphone");


            } elseif (empty($_POST['check']['1'])) {

                die("please ensure you have switched back to factory settings");
            } elseif (empty($_POST['check']['2'])) {
                die("please accept that Forza has no responsibility over loss of data");

            }
        }




               // if (empty($_POST['check']['lock'])) {

                  //  die("please ensure you have removed ICloud lock and turned off Find My Iphone");


                //}
                //elseif (empty($_POST['check'][''])) {
                /* $validate = Device::validateIMEI($_POST['IMEI']);
                 //$IMEI = $_POST['IMEI'];
                  }

                  //if (empty($_POST['check'])) {
                 // echo "No options were checked";


                // } else {

                  echo "check passed!";
                  $checked = 1;
                 $date = date('Y-m-d');
                 // $query = RebuyModel::checksubmit($IMEI, $checked, $order_id, $date);
                  //$status = RebuyModel::updateFStatus(4, $order_id);
                 // $action = RebuyModel::setAction(6, $order_id);

                  } else {
                  echo "the device did not pass check, please see notes.";
                 $checked = 0;
                  $date = date('Y-m-d');
                 $query = RebuyModel::checksubmit($IMEI, $checked, $order_id, $date);
                  $status = RebuyModel::updateFStatus(5, $order_id);
                  $action = RebuyModel::setAction(4, $order_id);


                  }*/
           // }
        }


    public function rmasteps()
    {

        View::renderTemplate('RMAController/rmasteps.html');
    }

    public function inspect()
    {
        View::renderTemplate('RMAController/inspect.html');


    }

     public function rmafailcard()
     {
         $rma_id=$this->activateAction();
         $imei=$_SESSION['imei'];
         $date=date('Y-m-d');
         View::renderTemplate('RMAController/rmafailcard.html');
         echo '<pre>';
         print_r($_POST);
         /**$failcard=$_POST['failcard'];**/
         function IsChecked($chkname,$value)
         {
             if(!empty($_POST[$chkname]))
             {
                 foreach($_POST[$chkname] as $chkval)
                 {
                     if($chkval == $value)
                     {
                         return true;
                     }
                 }
             }
             return false;
         }
         if(isset($_POST['sound'])){
             print_r($_POST['sound']);
             if(IsChecked('sound','30'))
             {
                 $speakers=1;
             }else{
                 $speakers=0;
             }
             if(IsChecked('sound','7'))
             {
                 $front_speaker=1;
             }else{
                 $front_speaker=0;
             }
             if(IsChecked('sound','24'))
             {
                 $microphone_bottom=1;
             }else{
                 $microphone_bottom=0;
             }
             if(IsChecked('sound','26'))
             {
                 $internal_speakers=1;
             }else{
                 $internal_speakers=0;
             }
             if(IsChecked('sound','13'))
             {
                 $microphone_back=1;
             }else{
                 $microphone_back=0;
             }
             if(IsChecked('sound','3'))
             {
                 $microphone_top=1;
             }else{
                 $microphone_top=0;
             }

            echo $sound=RMAModel::makeRMASoundInspection($rma_id,$imei,$date,$speakers,$internal_speakers,$microphone_bottom,$microphone_back,$front_speaker,$microphone_top);
         }else{
             echo 'no sound issues';
         }

            if(isset($_POST['power']))
            {
            print_r($_POST['power']);
                if(IsChecked('power','29'))
                {
                    $power=1;
                }else{
                    $power=0;
                }

                if(IsChecked('power','23'))
                {
                    $battery=1;
                }else{
                    $battery=0;
                }

                if(IsChecked('power','12'))
                {
                    $dock_connector=1;
                }else{
                    $dock_connector=0;
                }

                echo $powerinsp=RMAModel::makeRMAPowerInspection($rma_id,$imei,$date,$battery,$dock_connector,$power);
             }else{

                echo "no power issues";
            }

            if(isset($_POST['screen']))
            {
                print_r($_POST['screen']);
                if(IsChecked('screen','28'))
                {
                    $LCD=1;
                }else{
                    $LCD=0;
                }
                if(IsChecked('screen','11'))
                {
                    $multi_touch=1;
                }else{
                    $multi_touch=0;
                }
                if(IsChecked('screen','0'))
                {
                    $img_quality=1;
                }else{
                    $img_quality=0;
                }
                if(IsChecked('screen','6'))
                {
                    $ambient_light=1;
                }else{
                    $ambient_light=0;
                }
                if(IsChecked('screen','5'))
                {
                    $auto_brightness=1;
                }else{
                    $auto_brightness=0;
                }
                if(IsChecked('screen','4'))
                {
                    $proximity=1;
                }else{
                    $proximity=0;
                }

            echo $screen=RMAModel::makeRMAScreenInspection($rma_id,$imei,$date,$LCD,$multi_touch,$img_quality,$ambient_light,$auto_brightness,$proximity);
            }else{

                echo "No screen problems";
            }

            if(isset($_POST['buttons']))
            {
               print_r($_POST['buttons']);

                if(IsChecked('buttons','25'))
                {
                    $headset_jack=1;
                }else{
                    $headset_jack=0;
                }
                if(IsChecked('buttons','21'))
                {
                    $power_button=1;
                }else{
                    $power_button=0;
                }


                if(IsChecked('buttons','16'))
                {
                    $home_button=1;
                }else{
                    $home_button=0;
                }


                if(IsChecked('buttons','9'))
                {
                    $volume_flex_cable=1;
                }else{
                    $volume_flex_cable=0;
                }

                if(IsChecked('buttons','8'))
                {
                    $touch_id=1;
                }else{
                    $touch_id=0;
                }

            echo $buttons=RMAModel::makeRMAButtonsInspection($rma_id,$imei,$date,$headset_jack,$power_button,$volume_flex_cable,$home_button,$touch_id);
            }else{

                echo "no jacks/buttons problems";
            }

            If(isset($_POST['connections']))
            {
               Print_r($_POST['connections']);
                if(IsChecked('connections','17'))
                {
                    $SIM_fail=1;
                }else{
                    $SIM_fail=0;
                }
                if(IsChecked('connections','18'))
                {
                    $no_cell_conn=1;
                }else{
                    $no_cell_conn=0;
                }
                if(IsChecked('connections','19'))
                {
                    $signal_strength=1;
                }else{
                    $signal_strength=0;
                }
                if(IsChecked('connections','20'))
                {
                    $wifi_bt=1;
                }else{
                    $wifi_bt=0;
                }
                echo $conns=RMAModel::makeRMAConnsInspection($rma_id,$imei,$date,$wifi_bt,$signal_strength,$no_cell_conn,$SIM_fail);


            }else{
                echo "no connections problems";
            }

            if(isset($_POST['misc']))
            {
                print_r($_POST['misc']);

                if(IsChecked('misc','27'))
                {
                    $vibration_motor=1;
                }else{
                    $vibration_motor=0;
                }

                if(IsChecked('misc','22'))
                {
                    $GPS=1;
                }else{
                    $GPS=0;
                }

                if(IsChecked('misc','14'))
                {
                    $torch=1;
                }else{
                    $torch=0;
                }

                echo $misc=RMAModel::makeRMAMiscInspection($rma_id,$imei,$date,$vibration_motor,$GPS,$torch);

            }else{
                echo "no misc problems";
            }

            if(isset($_POST['camera']))
            {
                print_r($_POST['camera']);

                if(IsChecked('camera','15'))
                {
                    $rear_camera=1;
                }else{
                    $rear_camera=0;
                }
                if(IsChecked('camera','2'))
                {
                    $front_camera=1;
                }else{
                    $front_camera=0;
                }



                if(IsChecked('camera','1'))
                {
                    $fcfc=1;
                }else{
                    $fcfc=0;
                }

                echo $cam=RMAModel::makeRMACameraInspection($rma_id,$imei,$date,$rear_camera,$front_camera,$fcfc);
            }else{

             echo "no camera problems";
            }




         if(IsChecked('failcard','12'))
         {
             $physical_damage=1;
         }else{
             $physical_damage=0;
         }



         if(IsChecked('failcard','16'))
         {
             $charging=1;
         }else{
             $charging=0;
         }

         if(IsChecked('failcard','20'))
         {
             $power_flex_cable=1;
         }else{
             $power_flex_cable=0;
         }


         $date=date('Y-m-d');
        echo $date;
        echo $imei;
       echo $status=RMAModel::setRMAStatus(4,$rma_id);
        echo "created history event ". $event=EventModel::createHistoryEvent($imei,1);
       

     }

      public function dorepair()
      {
          $rma_id=$this->activate();
          $imei=$_SESSION['imei'];
          $results=RMAModel::getRMA($rma_id);
          $hiss=DeviceModel::getHistory($imei);
          $sn=RMAModel::checkSoundInspection($rma_id);
          $scn=RMAModel::checkScreenInspection($rma_id);
          //echo $scn;
          $pwr=RMAModel::checkPowerInspection($rma_id);
          //echo $pwr;
          $msc=RMAModel::checkMiscInspection($rma_id);
          //echo $msc;
          $conn=RMAModel::checkConnInspection($rma_id);
          //echo $conn;

         $warrantys=RMAModel::getWarranty($imei);
          //if($sn!=0 && $scn!=0 && $pwr!=0)
          //{
              $sounds=RMAModel::getSoundInspection($rma_id);
              $screens=RMAModel::getScreenInspection($rma_id);
              $powers=RMAModel::getPowerInspection($rma_id);
              $miscs=RMAModel::getMiscInspection($rma_id);
              $cons=RMAModel::getConnInspection($rma_id);
              $cams=RMAModel::getCameraInspection($rma_id);
              $buttons=RMAModel::getButtonsInspection($rma_id);
            //echo '<pre>';
          //foreach($sounds as $key => $link)
          //{
              //if($link !== '1')
              //{
                 //unset($sounds[$key]);
              //}
              //return $sounds;
         // }
        // print_r($sounds);



            $_SESSION['device']=  $message=$warrantys[0]['device_model'];

          View::renderTemplate('RMAController/dorepair.html',
                  ['results'=>$results,
                      'sounds'=>$sounds,
                      'screens'=>$screens,
                      'powers'=>$powers,
                     'miscs'=>$miscs,
                     'cons'=>$cons,
                      'cams'=>$cams,
                      'buttons'=>$buttons,
                      'hiss'=>$hiss,
                      'warrantys'=>$warrantys,
                      'message'=>$message
              ]);
          //}else {
              //View::renderTemplate('RMAController/dorepair.html',
                //  ['results' => $results]);
         // }
      }

    public function dorepairtwo()
    {
        $rma_id=$this->activate();
        $imei=$_SESSION['imei'];
        $device=$_SESSION['device'];
        $results=RepairModel::getrepair2parts();
        View::renderTemplate('RMAController/dorepairtwo.html',[
            'rma_id'=>$rma_id,
            'imei'=>$imei,
            'device'=>$device,
            'results'=>$results

        ]);
    }

    public function repairdone()
    {
        $rma_id=$this->activate();
        $message="meow";
        echo $status=RMAModel::setRMAStatus(5,$rma_id);
        $imei=$_SESSION['imei'];
        $device=$_SESSION['device'];
        $date=date('Y-m-d');
        //echo '<pre>';
        //print_r($_POST);
        View::renderTemplate('RMAController/repairdone.html',[
            'message'=>$message,
            'imei'=>$imei,
            'device'=>$device,
            'date'=>$date
        ]);
    }

    public function rmashipping()
    {
        $results = RMAModel::getShipping();
        View::renderTemplate('RMAController/rmashipping.html',[
            'results'=>$results
        ]);

    }

    public function editshipping()
    {

    }

    public function apicheck()
    {
        View::renderTemplate('RMAController/apicheck.html');

    }

    public function checkimeiapi()
    {
        $imei=$_POST['imei'];
        if (empty($_POST['imei'])) {

            die("IMEI not present. Please go back and enter it.");


        } else {
            $validate = Device::validateIMEI($_POST['imei']);
            $mei = $_POST['imei'];

        }
        $results=RMAModel::getWarranty($imei);
        View::renderTemplate('RMAController/checkimeiapi.html'
           ,
            [
            'imei'=>$imei]
        );
        echo '<pre>';
       //print_r($results);
        $date=date('Y-m-d');
        if($results[0][9]>$date)
        {
            echo 'warranty valid. Please <a href="'.$imei.'/'.'testorder">click here to start an RMAController </a>';
        }else{
            echo 'warranty ended. You can start a <a href='.'"https://www.forza-refurbished.nl/reparatiepaid"'.'> repair order</a>'.' instead';
            
        }
    }


}