<?php
/**
 * Created by PhpStorm.
 * UserController: SimonaThrussell
 * Date: 01/10/2018
 * Time: 09:41
 */
namespace App\Controllers;
use \Core\View;
use App\Models\LogisticsModel;
require '..\vendor\autoload.php';
class LogisticsController extends \Core\Controller
{
    public function indexAction()
    {


        View::renderTemplate('LogisticsController./index.html'

        );

    }
}