<?php
/**
 * Created by PhpStorm.
 * UserController: SimonaThrussell
 * Date: 01/10/2018
 * Time: 09:41
 */
namespace App\Controllers;
use App\Models\DeviceModel;
use \Core\View;
use App\Models\RepairModel;

require '..\vendor\autoload.php';
class RepairController extends \Core\Controller
{
    public function indexAction()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('RepairController/index.html'
            //['results'=>$results]
        );

    }

    public function activate()
    {
        $id = $this->route_params['id'];
        return $id;
    }

    public function getfailcard()
    {
        $order_id=$this->activate();

        $lookup=DeviceModel::getIMEI($order_id);
        $imei=$lookup[0]['IMEI'];
        $results=RepairModel::getfailcard($imei);
        View::renderTemplate('RepairController/index.html',
        ['results'=>$results]
        );


    }
}