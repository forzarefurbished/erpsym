<?php
/**
 * Created by PhpStorm.
 * UserController: SimonaThrussell
 * Date: 01/10/2018
 * Time: 09:28
 */
namespace App\Controllers;
use \Core\View;
use App\Models\SalesModel;
require '..\vendor\autoload.php';

class SalesController extends \Core\Controller
{
    public function index()
    {

        //$results = RebuyModel::getOrders();
        View::renderTemplate('SalesController/index.html'
            //['results'=>$results]
        );

    }
}