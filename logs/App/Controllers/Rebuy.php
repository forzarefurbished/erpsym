<?php
/**
 * Created by PhpStorm.
 * UserController: simona
 * Date: 28/09/2018
 * Time: 16:57
 */

namespace App\Controllers;
use \Core\View;
use \Core\Customer;
use \Core\Order;
use \Core\Device;
use App\Models\RebuyModel;
use App\Models\CustomerModel;
use App\Models\OrderModel;
use App\Models\FinanceModel;
use App\Models\DeviceModel;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
//local setup
require 'C:\xampp2\htdocs\FORZAERP\erpdemo\vendor\autoload.php';
//require 'C:\xampp\htdocs\FORZAERP\vendor\autoload.php';
class Rebuy extends \Core\Controller
{
    public function indexAction()
    {


        $results=rebuyModel::getStatus();
        View::renderTemplate('RebuyController/index.html', [

            'results'=> $results
        ]);

    }

    public function activateAction()
    {
        $id = $this->route_params['id'];
        return $id;
    }

    public function latestordersAction()
    {
        $results = RebuyModel::getOrders();
        View::renderTemplate('RebuyController/latestorders.html', [
            'results' => $results,

        ]);

    }

    public function inspectionAction()
    {
        $results = RebuyModel::getInspection();
        View::renderTemplate('RebuyController/inspection.html', [
            'results' => $results
        ]);

    }

    public function inspectAction()
    {
        $order_id=$this->activateAction();

        $results = RebuyModel::getDevice($order_id);
        View::renderTemplate('RebuyController/inspect.html', [
            'results' => $results
        ]);
        $_SESSION['order_id']=$order_id;

    }

    public function editAction()
    {
        $results = RebuyModel::getOrders();
        View::renderTemplate('RebuyController/edit.html', [
            'results' => $results
        ]);
    }

    public function mailaction()
    {

        $results = RebuyModel::getQuoted();
        View::renderTemplate('RebuyController/mail.html', [
            'results' => $results
        ]);
        //use php mailer script in rebuy/mail.php

    }

    public function sendmailAction()
    {
        $order_id=$this->activateAction();
            $results = RebuyModel::getQuote($order_id);
            View::renderTemplate('RebuyController/sendmail.html', [
                'results' => $results
            ]);

    }

    public function inspectsubmitAction()

    {
        $order_id=$_SESSION['order_id'];

        //$order_id=$this->activateAction();


        $results = RebuyModel::getInspectionById($order_id);
        View::renderTemplate('RebuyController/inspectsubmit.html',[
                'results'=>$results

            ]
        );






        $devicetype = $_POST['device_type'];
        $devicestorage = $_POST['device_storage'];
        $deviceconnection = $_POST['device_connection'];
        $devicecondition = $_POST['device_condition'];
        $devicecolour = $_POST['device_colour'];
        //$images=$_POST['images'];
        $devicecomments = $_POST['device_comments'];
        $query = RebuyModel::InspectSubmit($order_id, $devicetype, $devicestorage, $deviceconnection, $devicecondition, $devicecolour, $devicecomments);
        $status=RebuyModel::updateFStatus(16,$order_id);
        $action=RebuyModel::setAction(7,$order_id);


        //$failcard=$_POST['failcard'];
        if (!isset($_POST['failcard'])) {


            echo "no parts require  replacement";

        } else {
            echo '<pre>';
            //var_dump($_POST['failcard']);
           // echo "See below for parts required";
            $failcard = $_POST['failcard'];
            echo '<pre>';
            echo '<h3>Parts Needed </h3></br>';
            foreach ($failcard as $value) {
                echo $value . "</br>";
            }


        }





    }

    public function statusAction()
    {
        $results = RebuyModel::getStatus();
        View::renderTemplate('RebuyController/status.html', [
            'results' => $results
        ]);

    }







    public function mailsentAction()
    {
        $order_id=$this->activateAction();
        View::renderTemplate('RebuyController/mailsent.html');

        if(isset($_POST['submit'])) {
          $name=$_POST['name'];
          $email=$_POST['email'];
          $subject="Offer from Forza";
          $quote=$_POST['msg'];
          $body="Thank you for sending us your device. 
          We would like to offer you \r\n $quote \r\n euro for it". 'To accept or refuse please visit <a href='.'"'.'http://forzaerp.local/rebuy/'.$order_id.'/'.'acceptquote'.'"'.'>This Link</a>';
            echo '<pre>';
            var_dump($_POST);

            $mail = new PHPMailer(TRUE);

            try {

                $mail->setFrom('simona.thrussell@forza-refurbished.nl', $name);
                $mail->addAddress($email, 'your name');
                $mail->Subject = $subject;
                $mail->Body = $body;

                /* SMTP parameters. */
                $mail->isSMTP();
                $mail->Host = 'smtp.office365.com';
                $mail->SMTPAuth = TRUE;
                $mail->SMTPSecure = 'tls';
                $mail->Username = 'simona.thrussell@forza-refurbished.nl';
                $mail->Password = 'DcadkA7h';
                $mail->Port = 587;

                /* Disable some SSL checks. */
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );

                /* Finally send the mail. */
                $mail->send();
                $status=RebuyModel::updateFStatus(9,$order_id);
                $action=RebuyModel::setAction(14,$order_id);
            }
            catch (Exception $e)
            {
                echo $e->errorMessage();
            }



            }
            else{

            echo "no input submitted";

        }

    }

    public function checkordersAction()
    {
        $results = RebuyModel::getCheck();
        View::renderTemplate('RebuyController/checkorders.html', [
            'results' => $results
        ]);


    }

    public function checkAction()
    {
        $order_id=$this->activateAction();
        $results = RebuyModel::getDevice($order_id);
        View::renderTemplate('RebuyController/check.html');
    }

    public function checksubmitAction()
    {
        $order_id = $this->activateAction();

        $results=RebuyModel::getActionButtons($order_id);
        View::renderTemplate('RebuyController/checksubmit.html',['results'=>$results]);
        echo '<pre>';
        //var_dump($_POST['check']);
        if (isset($_POST['check'])) {
            if (empty($_POST['IMEI'])) {

                die("IMEI not present. Please go back and enter it.");


            } else {
                $validate = Device::validateIMEI($_POST['IMEI']);
                $IMEI = $_POST['IMEI'];

            }

            if (empty($_POST['check'])) {
                echo "No options were checked";


            } else {
                $check = $_POST['check'];


                $N = count($check);
                if ($N == 3) {
                    echo "check passed!";
                    $checked = 1;
                    $query = RebuyModel::checksubmit($IMEI, $checked, $order_id);
                    $status = RebuyModel::updateFStatus(4, $order_id);
                    $action = RebuyModel::setAction(6, $order_id);


                } else {
                    echo "the device did not pass check, please see notes.";
                    $checked = 0;
                    $query = RebuyModel::checksubmit($IMEI, $checked, $order_id);
                    $status = RebuyModel::updateFStatus(5, $order_id);
                    $action = RebuyModel::setAction(4, $order_id);


                }
            }


        }else{

            echo "no data was submitted";
        }


    }

    public function reportsAction()
    {
        View::renderTemplate('RebuyController/reports.html'
            //, [
          //  'results' => $results
       //]
        );

    }

    public function quoteAction()
    {
        $order_id=$this->activateAction();
        $results = RebuyModel::getDevice($order_id);
        View::renderTemplate('RebuyController/quote.html', [
            'results' => $results
        ]);

    }


    public function quoteordersAction()
    {
        $results = RebuyModel::getInspected();
        View::renderTemplate('RebuyController/quoteorders.html', [
            'results' => $results
        ]);

    }


    public function submitquoteAction()
    {
        $order_id=$this->activateAction();
       $quote = $_POST['quote'];
        $query = RebuyModel::SubmitQuote($order_id,$quote);
        $status=RebuyModel::updateFStatus(17,$order_id);
        $action=RebuyModel::setAction(8,$order_id);
       $results = RebuyModel::getQuote($order_id);
        View::renderTemplate('RebuyController/submitquote.html',
             [
            'results' => $results]);


    }

    public function overviewAction()
    {

        $results = RebuyModel::Overview();
        View::renderTemplate('RebuyController/overview.html', [
         'results' => $results
         ]
        );

    }


    public function enterorderAction()
    {

        View::renderTemplate('RebuyController/enterorder.html');




    }

    public function entersubmitAction()
    {

        View::renderTemplate('RebuyController/entersubmit.html');

        $devicetype = $_POST['device_type'];
        $devicestorage = $_POST['device_storage'];
        $deviceconnection = $_POST['device_connection'];
        $devicecondition = $_POST['device_condition'];
        $devicecolour = $_POST['device_colour'];

        $firstname=$_POST['first_name'];
        $lastname=$_POST['last_name'];
        $email=$_POST['email'];
        $phone=$_POST['phone'];
        $customer_type=$_POST['customer_type'];
        $postcode=$_POST['postcode'];
        $streetnumber=$_POST['street_number'];
       $addition=$_POST['addition'];
       $streetname=$_POST['street_name'];
        $city=$_POST['city'];
      $country=$_POST['country'];

        $paymenttype=$_POST['payment'];
        $IBAN=$_POST['IBAN'];
        $Tnv=$_POST['Tnv'];


       $customer_id= CustomerModel::createCustomer($firstname,$lastname,$email,$phone, $customer_type);
        echo '<pre>';
       echo $customer_id;
       $query=CustomerModel::createAddress($customer_id,$postcode,$streetnumber,$addition,$streetname,$city,$country);
       // RebuyModel::OrderSubmit($order_id, $devicetype, $devicestorage, $deviceconnection, $devicecondition, $devicecolour);
        echo $query;
        $order_id=OrderModel::createOrder($customer_id,$paymenttype);
       echo $order_id;
       //var_dump($_POST);
      echo  $device=DeviceModel::makeRebuyDevice($order_id,$devicetype,$devicestorage,$deviceconnection,$devicecondition,$devicecolour);
      echo $status=RebuyModel::makeStatus($order_id)."</br>";
      echo $shipping=RebuyModel::createShippingStatus($order_id);
        echo $payment=FinanceModel::createPaymentData($customer_id,$order_id,$IBAN,$Tnv);

    }

    public function serviceAction()
    {
        View::renderTemplate('RebuyController/service.html');

    }

     public function setstatusAction()
     {
         $order_id=$this->activateAction();
         $results=RebuyModel::setStatus($order_id);
         View::renderTemplate('RebuyController/setstatus.html', [
             'results' => $results
         ]);
         echo '<pre>';
         var_dump($results);
        foreach($results as $result){
          $co=$results["customer_order_status"];
          $fo=$results["forza_order_status"];
          echo $co;
          echo $fo;




        }


     }

    public function acceptQuoteAction()
    {
        $order_id=$this->activateAction();
        $_SESSION['order_id']=$order_id;
        $results=RebuyModel::getQuote($order_id);
        View::renderTemplate('RebuyController/acceptquote.html',[
            'results'=>$results
        ]);




    }


    public function confirmAction()
    {
        //$order_id=$this->activateAction();
        $order_id=$_SESSION['order_id'];
        echo $order_id;

        $date=date('d-m-Y');
        echo $date;
        View::renderTemplate('RebuyController/confirm.html');
        $update=RebuyModel::setAccepted($order_id,$date);
        $action=RebuyModel::setAction(9,$order_id);
        $fstatus=RebuyModel::updateFStatus(11,$order_id);
        $status=RebuyModel::updateCStatus(4,$order_id);



    }

    public function refuseoptionsAction()
    {
        View::renderTemplate('RebuyController/refuseoptions.html');

    }


    public function addtagsAction()
    {

        $results = RebuyModel::Overview();
        View::renderTemplate('RebuyController/addtags.html', [
                'results' => $results
            ]
        );


    }

    public function setStatus()
    {
        //$order_id=$this->activateAction();
        $results = RebuyModel::getStatus();

        foreach ($results as $result) {
            $order_id = $result['order_id'];
            $cstatus = $result['customer_order_status'];
            $fstatus = $result['status_id'];
            $action = $result['next_action_id'];
            $update=self::setAction($cstatus,$fstatus);



        }
        View::renderTemplate('RebuyController/setstatus.html', [
                'results' => $results
            ]
        );
    }

    public function setAction($cstatus,$fstatus)
    {


        //$action;
        $status = RebuyModel::setAction($action, $order_id);
    }


    public function setQuote()
    {




    }



    public function devicereceivedAction()
    {
        $results = RebuyModel::Shipping();
        View::renderTemplate('RebuyController/devicereceived.html', [
                'results' => $results
            ]
        );


    }

     public function ordereditAction()
     {
         $order_id=$this->activateAction();
         $results = RebuyModel::GetOrders();
         View::renderTemplate('RebuyController/devicereceived.html', [
                 'results' => $results
             ]
         );

     }

     public function shippingAction()
     {
         $results = RebuyModel::getShipping();
         View::renderTemplate('RebuyController/shipping.html', [
                 'results' => $results
             ]
         );



     }

     public function editshippingAction()
     {
         $order_id=$this->activateAction();
         $_SESSION['order_id']=$order_id;
         $results = RebuyModel::getShippingById($order_id);
         View::renderTemplate('RebuyController/editshipping.html', [
                 'results' => $results
             ]
         );


     }

     public function shippingupdateAction()
     {
         //$order_id=$this->activateAction();
         $order_id=$_SESSION['order_id'];
         if(isset($_POST['submit'])) {
             //var_dump($_POST['submit']);
             $status = $_POST['status'];
             $query=RebuyModel::updateShipping($status, $order_id);

             switch($status) {
                 case 3:
                     $fstatus = 3;
                     $query = RebuyModel::updateFStatus(3, $order_id);
                     $update= RebuyModel::setAction(2, $order_id);
                     break;

                 case 7:
                     $action = 1;
                     $query = RebuyModel::updateFStatus(7, $order_id);
                     $query2 = RebuyModel::setAction(1, $order_id);
                     break;

             }

         }
         else{

             echo "no info was sent";
         }
         $results = RebuyModel::getShippingById($order_id);
         View::renderTemplate('RebuyController/shippingupdate.html', [
                 'results' => $results
             ]
         );


     }


     public function secondOfferAction()
     {
         //$order_id=$this->activateAction();
         $results = RebuyModel::GetReturns();
         View::renderTemplate('RebuyController/secondOffer.html', [
             'results' => $results
         ]);



     }

     public function submitsecondquoteAction()
     {
         $order_id=$this->activateAction();
         //$_SESSION['order_id'];
         $results=RebuyModel::retrievesecondquote($order_id);

         $order_id=$_SESSION['order_id'];
        // var_dump($_POST);
         $_SESSION['order_id']=$order_id;
         if(isset($_POST['submit'])) {
             $quote=$_POST['quote'];
            echo $query = RebuyModel::submitSecondQuote($order_id, $quote);
            $action=RebuyModel::setAction(10,$order_id);
         }else{

             echo "please enter new offer";
         };

         View::renderTemplate('RebuyController/submitsecondquote.html',[
             'results'=>$results

         ]);
     }

     public function entersecondofferAction()
     {
         $order_id=$this->activateAction();
         //$order_id=$_SESSION['order_id'];
         $_SESSION['order_id']=$order_id;
         $results=RebuyModel::retrievefirstoffer($order_id);
         View::renderTemplate('RebuyController/entersecondoffer.html',[
             'results' => $results
         ]);


     }

     public function sendsecofferAction()
     {
        $order_id=$this->activateAction();
         //$order_id=$_SESSION['order_id'];
        $results=RebuyModel::retrievesecondquote($order_id);
         View::renderTemplate('RebuyController/sendsecoffer.html',[
             'results' => $results
         ]);

     }

     public function ftstatusreportsAction()
     {
         



     }

     public function offersAction()
     {
        $results=RebuyModel::getOffers();
         View::renderTemplate('rebuy/offers.html'
             ,[
             'results' => $results
         ]
         );


     }

    public function paymentsAction()
    {
        $results=RebuyModel::getAcceptedOffers();
        View::renderTemplate('rebuy/payments.html'
            ,[
                'results' => $results
            ]
        );


    }


    public function returnsAction()
    {
        $results=RebuyModel::getReturns();
        View::renderTemplate('rebuy/returns.html'
            ,[
                'results' => $results
            ]
        );


    }

    public function recycleAction()
    {
        $order_id=$_SESSION['order_id'];
        View::renderTemplate('RebuyController/recycle.html');
        $status=RebuyModel::updateCStatus(6,$order_id);
        $fstatus=RebuyModel::updateFStatus(8,$order_id);
        $action=RebuyModel::setAction(12,$order_id);


    }

    public function closeAction()
    {
        $results=RebuyModel::getClose();
        View::renderTemplate('RebuyController/close.html'
            ,[
                'results' => $results
            ]
        );


    }

    public function closeorderAction()
    {
        $order_id=$this->activateAction();
        $results=RebuyModel::getOrderToClose($order_id);
        View::renderTemplate('RebuyController/closeorder.html',[
            'results'=>$results

        ]);

    }

    public function ordercloseAction()
    {
        $order_id=$this->activateAction();
        if(isset($_POST['submit']))
        {
            $result=RebuyModel::closeOrder($order_id);
        }
        View::renderTemplate('RebuyController/orderclose.html');


    }


    public function acceptedAction()
    {
        View::renderTemplate('RebuyController/accepted.html');

    }

    public function payAction()
    {
        $order_id=$this->activateAction();
        $results=RebuyModel::getPaymentDetails($order_id);
        View::renderTemplate('RebuyController/pay.html',[
            'results'=>$results

        ] );


    }

    public function sendAction()
    {
        $order_id=$this->activateAction();
        $results=RebuyModel::getPaymentDetails($order_id);
        $status=RebuyModel::updateFStatus(11,$order_id);
        View::renderTemplate('RebuyController/send.html'
            ,[
            'results'=>$results

        ]

    );
        var_dump($results);


    }

    public static function refusedoffersAction()
    {
        $results=RebuyModel::getRefused1();
        View::renderTemplate('RebuyController/refusedoffers.html'
            ,[
            'results'=>$results

        ]);

    }

    public static function recycleordersAction()
    {
        $results=RebuyModel::getRecycle();

        View::renderTemplate('RebuyController/recycleorders.html'
            ,[
                'results' => $results
            ]
        );

    }

    public function returndeviceAction()
    {
        $order_id=$_SESSION['order_id'];
        View::renderTemplate('RebuyController/returndevice.html');
        $status=RebuyModel::updateCStatus(12,$order_id);
        $action=RebuyModel::setAction(11,$order_id);


    }

    public function sendsecondoffermailAction()

         {
             $order_id=$this->activateAction();
             View::renderTemplate('RebuyController/sendsecondoffermail.html');

             if(isset($_POST['submit'])) {
                 $name=$_POST['name'];
                 $email=$_POST['email'];
                 $subject="Second Offer from Forza";
                 $quote=$_POST['msg'];
                 $body="Thank you for sending us your device. 
                We would like to offer you \r\n $quote \r\n euro for it". 'To accept or refuse please visit <a href='.'"'.'http://forzaerp.local/rebuy/'.$order_id.'/'.'acceptsecquote'.'"'.'>This Link</a>';
                 echo '<pre>';
                 var_dump($_POST);

                 $mail = new PHPMailer(TRUE);

                 try {

                     $mail->setFrom('simona.thrussell@forza-refurbished.nl', $name);
                     $mail->addAddress($email, 'your name');
                     $mail->Subject = $subject;
                     $mail->Body = $body;

                     /* SMTP parameters. */
                     $mail->isSMTP();
                     $mail->Host = 'smtp.office365.com';
                     $mail->SMTPAuth = TRUE;
                     $mail->SMTPSecure = 'tls';
                     $mail->Username = 'simona.thrussell@forza-refurbished.nl';
                     $mail->Password = 'DcadkA7h';
                     $mail->Port = 587;

                     /* Disable some SSL checks. */
                     $mail->SMTPOptions = array(
                         'ssl' => array(
                             'verify_peer' => false,
                             'verify_peer_name' => false,
                             'allow_self_signed' => true
                         )
                     );

                     /* Finally send the mail. */
                     $mail->send();
                     $status=RebuyModel::updateFStatus(10,$order_id);
                     $action=RebuyModel::setAction(14,$order_id);
                 }
                 catch (Exception $e)
                 {
                     echo $e->errorMessage();
                 }



             }
             else{

                 echo "no input submitted";

             }

         }


    public function sendsecmailAction()
    {
        $order_id=$_SESSION['order_id'];
        $results=RebuyModel::getSecQuote($order_id);

        View::renderTemplate('RebuyController/sendsecmail.html',['results'=>$results]);




    }

    public function acceptsecquoteAction()
    {
             $order_id=$this->activateAction();
             $_SESSION['order_id']=$order_id;
             $results=RebuyModel::retrievesecondquote($order_id);
             View::renderTemplate('RebuyController/acceptsecquote.html',[
                 'results'=>$results
             ]);


    }

    public function secconfirmAction()
    {
        $order_id=$_SESSION['order_id'];

        $results=RebuyModel::getQuote($order_id);
        //echo $order_id;
        $offer_type='second offer';
        $date = date('Y-m-d');


        $offer=new Offer($results[0][21],$order_id,$results[0][14],$date,$offer_type,1);
        View::renderTemplate('RebuyController/secconfirm.html');
       // $update=RebuyModel::setAccepted($order_id,$date);
        $action=RebuyModel::setAction(9,$order_id);
        $fstatus=RebuyModel::updateFStatus(11,$order_id);
        $status=RebuyModel::updateCStatus(7,$order_id);


    }

    public function secrefuseoptionsAction()
    {
        $order_id=$_SESSION['order_id'];
        $results=RebuyModel::getQuote($order_id);
        //echo $order_id;
        $offer_type='second offer';
        $date = date('Y-m-d');


        $offer=new Offer($results[0][21],$order_id,$results[0][14],$date,$offer_type,0);
        View::renderTemplate('RebuyController/secrefuseoptions.html');

    }

    public function returnAction()
    {
        $order_id=$this->activateAction();
        $results=RebuyModel::getAddressbyOrderId($order_id);
        View::renderTemplate('RebuyController/return.html',['results'=>$results]);

    }

    public function failcardAction()
    {
        $order_id=$this->activateAction();
        $_SESSION['order_id']=$order_id;
        View::renderTemplate('RebuyController/failcard.html');
    }

    public function setfailcardAction()
    {
        //$order_id=$_SESSION['order_id'];

        $order_id=$this->activateAction();
        $results=RebuyModel::getOrderActionById($order_id);


       $IMEI= $results[0]['IMEI'];
        View::renderTemplate('RebuyController/setfailcard.html',['results'=>$results]);

        if(isset($_POST['submit']))
        {
            echo '<pre>';

               // print_r($_POST['failcard']);
                $failcard=1;



                function IsChecked($chkname,$value)
                {
                    if(!empty($_POST[$chkname]))
                    {
                        foreach($_POST[$chkname] as $chkval)
                        {
                            if($chkval == $value)
                            {
                                return true;
                            }
                        }
                    }
                    return false;
                }


                if(IsChecked('failcard','battery'))
                {
                    $battery=1;
                }else{
                    $battery=0;
                }

                if(IsChecked('failcard','speakers'))
                {
                    $speakers=1;
                }else{
                    $speakers=0;
                }

                if(IsChecked('failcard','speakers'))
                {
                    $lcd=1;
                }else{
                    $lcd=0;
                }

                if(IsChecked('failcard','camera'))
                {
                    $camera=1;
                }else{
                    $camera=0;
                }

                if(IsChecked('failcard','microphone'))
                {
                    $microphone=1;
                }else{
                    $microphone=0;
                }

                if(IsChecked('failcard','powerbutton'))
                {
                    $powerbutton=1;
                }else{
                    $powerbutton=0;
                }

                echo $battery."</br>";
                echo $speakers."</br>";
                echo $lcd."</br>";
                echo $camera."</br>";
                echo $microphone."</br>";
                echo $powerbutton."</br>";



            }else{
                $failcard=0;
            $battery=0;
            $speakers=0;
            $lcd=0;
            $camera=0;
            $microphone=0;
            $powerbutton=0;



        }
      echo $fail=RebuyModel::setFailcard($order_id,$IMEI,$failcard,$battery,$speakers,$lcd,$camera,$microphone,$powerbutton);



    }

    public function takeactionAction()
    {
        $order_id=$this->activateAction();
        $results=RebuyModel::getQuote($order_id);
        View::renderTemplate('RebuyController/takeaction.html',
          ['results'=>$results]

            );

    }

    public function returnorderAction()
    {
        $order_id=$this->activateAction();
        $results=RebuyModel::getQuote($order_id);
        View::renderTemplate('RebuyController/returnorder.html',
            ['results'=>$results]

        );

    }

     public function recycledeviceAction()
     {
         $order_id=$this->activateAction();
         $status=RebuyModel::updateFStatus(14,$order_id);
         $action=rebuyModel::setAction(13,$order_id);
         $results=RebuyModel::getOrderActionById($order_id);
         View::renderTemplate('RebuyController/recycledevice.html',
             ['results'=>$results]

         );

     }

}
















